package edu.Maman.lab1;

public class Card {
	private Suit suit;
	private Rank rank;
    
	public  static  boolean  compare(Card  c1,  Card  c2) {
		return(c1.suit==c2.suit && c1.rank==c2.rank);
	}
	public  static  boolean  compareRank(Card  c1,  Card  c2) {
		return(c1.rank==c2.rank);
	}
	
	public  static  boolean  compareSuit(Card  c1,  Card c2) {
		return(c1.suit==c2.suit);
	}

	public Card (Suit suit, Rank rank){
		this.suit=suit;
		this.rank=rank;
	}
	public Suit getSuit() {
		return suit;
		
	}
	public void setSuit (Suit suit){
		this.suit=suit;
	}
	public Rank getRank () {
		return rank;
	}
	public void setRank (Rank rank){
		this.rank=rank;
	}
	public String toString(){
		String s = "";
		return s + suit + " " + rank;
	}
}