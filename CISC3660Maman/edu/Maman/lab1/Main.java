package edu.Maman.lab1;

import java.util.ArrayList;

public class Main { 

     public static void main (String [] args){ //Test
        Deck deck = new Deck ();
        deck.shuffle();
        
        Card arr1[] = new Card[26], arr2[] = new Card[26];
        for(int i=0;i<26;i++)
        	arr1[i]=deck.dealCard();
        for(int i=0;i<26;i++)
        	arr2[i]=deck.dealCard();
        
        int sum1 = 0, sum2 = 0;
        for(int i=0;i<arr1.length;i++){
        	sum1+=arr1[i].getRank().ordinal();
        }
        for(int i=0;i<arr2.length;i++){
        	sum2+=arr2[i].getRank().ordinal();
        }
        System.out.print("The winner is: ");
        if(sum1>sum2) System.out.println("deck 1");
        else System.out.println("deck 2");
     }
}