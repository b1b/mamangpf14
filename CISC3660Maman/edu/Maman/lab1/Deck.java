package edu.Maman.lab1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random; 

public class Deck {
	//You could use a linkedList instead of an ArrayList.
    // It depends on the operations that you want to perform on 
	//the list.
	private ArrayList <Card> deck = new ArrayList<Card> ();
	
	//returns  all  the  cards  in  a  deck  ordered  by  Rank
	public  ArrayList<Card>  getOrderedCard() {
		ArrayList<Card> deck2 = new ArrayList<Card>();
		for (Rank r: Rank.values ()) {
			for (Suit s: Suit.values ()) {
				for(int i=0;i<deck.size();i++){
					if(Card.compare(deck.get(i), new Card(s,r))==true)
						deck2.add(deck.get(i));
				}
			}
		}
		return deck2;
	}
	//Returns  the  number  of  cards  in  the  deck. 
	public  int  getNumberOfCardsRemaining() {
		return deck.size();
	}
	
	public  Card  dealCard() {
		Random rand = new Random();
		int ind = rand.nextInt(deck.size());
		Card c = deck.get(ind);
		deck.remove(ind);
		return c;
	}
	
	
	public  void  shuffle() {
		Collections.shuffle(deck); //method in collections
	}

	
	public Deck() {
		for (Rank r: Rank.values ()) {
			for (Suit s: Suit.values ()) {
				deck.add (new Card (s, r));
			}
		}
		
	}
	
	public List <Card> getDeck ()
	{
		return Collections.unmodifiableList (deck);
	}
	
	public Card removeCardDeck (int index){
		return deck.remove (index);
	}
	
	public void addOneCard (Card c) {
	    deck.add (c); //Appends the new card to the end of the deck.
	}
	
	//Remove one card of same rank and suit
	public void removeAParticularCard (Card c) {
		for (int i=deck.size ()-1; i>=0; i--){
			if (deck.get(i).getRank() == c.getRank() && deck.get(i).getSuit() == c.getSuit ()){
				deck.remove (i);
				break;
			}
		}
	}
	public void removeAllCardsOfRank (Rank r) {
		for (int i=deck.size()-1; i>=0; i--){
			if (deck.get (i).getRank()==r){
				deck.remove (i);
			}
		}
	}
	@Override
	public String toString (){
		//Student, think about a more efficient way of doing this:
		String s= "";
		for (Card c: deck){
			s = s + c.getSuit().toString() + " " + c.getRank().toString()
					+ "\n";
		}
		return s;
		}
	}




