package edu.Maman.lab2;

public class Constants {
	final static String WAIT_MSG ="Wait for other players to finish their turn";
	final static String WRONG_TURN_MSG ="IT IS NOT YOUR TURN!";
	final static String WON = "You Won!";
	final static String LOST = "You Lost! :(";
	final static String BLACKJACK = "Blackjack!";
	public static final int MAX_NUM_PLAYERS = 4; 
}
