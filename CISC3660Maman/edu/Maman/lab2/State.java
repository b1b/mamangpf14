package edu.Maman.lab2;

import edu.Maman.lab1.Deck;

public class State {
	private byte turn; //1 2  3 
	private Player [] players; // players[0] is the dealer
	private Deck deck;
	public State(){
		init();
	}
	
	public void init() { //Initial state
		turn = 1;
		deck = new Deck();
		deck.initFullDeck();
		deck.shuffle();
		players = new Player[Constants.MAX_NUM_PLAYERS]; // dealer +3 players
		players[0] = new Player();
		players[0].receiveCard(deck.dealCard());

		// give 1 card to each players (as we deal another card in another message)
		for (int i = 1; i < Constants.MAX_NUM_PLAYERS; i++) {
			players[i]= new Player();
			players[i].receiveCard(deck.dealCard());
		}
	}
	
	public Player getPlayer(int playerNum){
		return players[playerNum];
	}

	// We could just use getPlayer but including this method improves readability
	public Player getDealer() {
		return players[0];
	}

	public Deck getDeck(){ return deck; }
	public byte getTurn(){ return turn; }
	public void nextTurn(){ turn++; }
}
