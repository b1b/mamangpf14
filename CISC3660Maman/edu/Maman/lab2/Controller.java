package edu.Maman.lab2;
import edu.Maman.lab1.Rank;

public class Controller {
	MessagePanel msg;
	boolean playing;
	State state;
	
	public Controller() {}
	
	public void init() {
		state = new State();
		//send message to player1
		msg.outputMessage(1, "It is your turn. Your hand; " 
				+ state.getPlayer(1).getHandString()
				+ " Dealer's Hand: "
				+ state.getDealer().getHandString());
		msg.outputMessage(2, Constants.WAIT_MSG);
		msg.outputMessage(3, Constants.WAIT_MSG);
	}
	
	public void clientRequest(char c){
		if (playing) {
			// Decide based on c
			changeState(c);
			return;
		}
		init();
			playing = true;
	}

	public void setMessenger(MessagePanel message) {
		msg = message;
		msg.showMenu();
	}

	private void showErrorMsgPlayerKey(char c) {
		int player = -1;
		if (c == 'q' || c == 'w') 		player = 1;
		else if (c == 'a' || c == 's') 	player = 2;
		else if (c == 'z' || c == 'x') 	player = 3;
		msg.outputMessage(player, Constants.WRONG_TURN_MSG);
	}
	
	private void changeState(char c) {
		boolean isPlayerOneTurn, isPlayerTwoTurn, isPlayerThreeTurn, isDealerTurn;
		switch(state.getTurn()){
			case 1:
				isPOneTurn=true;
				isPOneTurn = isPThreeTurn = isDTurn = false;
				break;
			case 2:
				isPTwoTurn=true;
				isPOneTurn = isPThreeTurn = isDTurn = false;
				break;
			case 3:
				isPThreeTurn=true;
				isPOneTurn = isPTwoTurn = isDTurn = false;
				break;
			default:
				isDTurn=true;
				isPOneTurn = isPTwoTurn = isPThreeTurn = false;
				break;
		}

		if (isPOneTurn){
			if(c == 'q') {
				turnAnnounce(1);
				handAnalyze(1);
			}
			else if (c == 'w') {
				state.nextTurn();
				turnAnnounce(state.getTurn());
			}
		}	
		else if (isPTwoTurn){
			if(c == 'a') {
				turnAnnounce(2);
				handAnalyze(2);
			}
			else if (c == 's') {
				state.nextTurn();
				turnAnnounce(state.getTurn());
			}
		}		
		else if (isPThreeTurn){
			if(c == 'z') {
				turnAnnounce(3);
				handAnalyze(3);
			}
			else if (c == 'x') {
				state.nextTurn();
				msg.outputMessage(3, Constants.WAIT_MSG);
				msg.outputMessage(4, "It is your turn. Your hand; " 
						+ state.getDealer().getHandString() + " Dealer's Hand: "
						+ state.getDealer().getHandString());
			}

		}
		else if (isDealerTurn) {		
			if (shouldDealerGetCard()) {
				state.getDealer().receiveCard(state.getDeck().dealCard());
				handAnalyze(4);
			}
			int player = maxHand();
			if (player == 0) {
				player = 4;
			}
			
			msg.outputMessage(player, Constants.WON);
		}

		else {
			showErrorMsgPlayerKey(c);
		}		
	}
	
	private void turnAnnounce(int player) {
		state.getPlayer(player).receiveCard(state.getDeck().dealCard());				
		msg.outputMessage(player, "It is your turn. Your hand; " 
				+ state.getPlayer(player).getHandString() + " Dealer's Hand: "
				+ state.getDealer().getHandString());
		
		if (player == 1) {
			msg.outputMessage(2, Constants.WAIT_MSG);
			msg.outputMessage(3, Constants.WAIT_MSG);
		} else if (player == 2) {
			msg.outputMessage(1, Constants.WAIT_MSG);
			msg.outputMessage(3, Constants.WAIT_MSG);
		} else if (player == 3) {
			msg.outputMessage(1, Constants.WAIT_MSG);
			msg.outputMessage(2, Constants.WAIT_MSG);
		}
	}
	
	private void handAnalyze(int player) {
		int handSize = state.getPlayer(player).getHand().size();
		int handTotal = 0;

		// Calculate total
        for (int i = 0; i < handSize; i++) {
        	Rank r=state.getPlayer(player).getHand().get(i).getRank();
        	if(r.equals(Rank.KING)||r.equals(Rank.QUEEN)||r.equals(Rank.JACK))
        		handTotal+=10;
        	else
        		handTotal += (state.getPlayer(player).getHand().get(i).getRank().ordinal() + 1);
        }

        if (handTotal > 21) {
        	msg.outputMessage(player, Constants.LOST);
			System.out.println("lost: " + handTotal);
			state.nextTurn();
        } else if (handTotal == 21) {
        	msg.outputMessage(player, Constants.WON);
			System.out.println("21!");
			state.nextTurn();
        }
	}
	
	private int maxHand() {
		int playerOneHand   = 0, playerTwoHand   = 0, playerThreeHand = 0, dealerHand = 0, max  = 0, maxPlayer = -1;

		// Calculate total hand size for each player
        for (int i = 0; i < state.getPlayer(1).getHand().size(); i++) {
        	Rank r=state.getPlayer(1).getHand().get(i).getRank();
        	if(r.equals(Rank.KING)||r.equals(Rank.QUEEN)||r.equals(Rank.JACK))
        		playerOneHand+=10;
        	playerOneHand += state.getPlayer(1).getHand().get(i).getRank().ordinal() + 1;
        }
        
        for (int i = 0; i < state.getPlayer(2).getHand().size(); i++) {
        	Rank r=state.getPlayer(2).getHand().get(i).getRank();
        	if(r.equals(Rank.KING)||r.equals(Rank.QUEEN)||r.equals(Rank.JACK))
        		playerTwoHand+=10;
        	else 
        		playerTwoHand += r.ordinal() + 1;
        }
        
        for (int i = 0; i < state.getPlayer(3).getHand().size(); i++) {
        	Rank r=state.getPlayer(3).getHand().get(i).getRank();
        	if(r.equals(Rank.KING)||r.equals(Rank.QUEEN)||r.equals(Rank.JACK))
        		playerThreeHand+=10;
        	else 
        		playerThreeHand += r.ordinal() + 1;
        }
        
        for (int i = 0; i < state.getDealer().getHand().size(); i++) {
	    	Rank r=state.getPlayer(0).getHand().get(i).getRank();
	    	if(r.equals(Rank.KING)||r.equals(Rank.QUEEN)||r.equals(Rank.JACK))
	    		dealerHand+=10;
	    	else 
	    		dealerHand += r.ordinal() + 1;
        }
        
        if (playerOneHand <= 21) {
        	maxPlayer = 1;
        	maxHandTotal = playerOneHand;
        }
        
        if (playerTwoHand <= 21 && playerTwoHand > maxHandTotal) {
        	maxPlayer = 2;
        	maxHandTotal = playerTwoHand;
	    }
        
        if (handTotalPlayerThree <= 21 && handTotalPlayerThree > maxHandTotal) {
	    	maxPlayer = 3;
        	maxHandTotal = handTotalPlayerThree;
	    }
        
        if (handTotalDealer <= 21 && handTotalDealer > maxHandTotal) {
	    	maxPlayer = 0;
        	maxHandTotal = handTotalDealer;
	    }
		
        return maxPlayer;        
	}
	
	private boolean shouldDealerGetCard() {
		int player = maxHand();
 
        if (player == 0) {
        	return false;
        } else {
        	return true;
        }
//		int sum =0;
//		for(int i=0;i<state.getDealer().getHand().size();i++)
//			sum+=state.getDealer().getHand().get(i).getRank().ordinal() + 1;
//		if(sum<17)
//			return true;
//		return false;
	}

}
