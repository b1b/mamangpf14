package edu.maman.lab3.gameobjects;

public interface Updatable {
	void update(float deltaTime);
}
