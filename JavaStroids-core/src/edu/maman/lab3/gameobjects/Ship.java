package edu.maman.lab3.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Ship extends GameObject implements Updatable{
	private Vector2 direction, targetDirection,velocity;
	private final float MIN_VELOCITY=20;
	public Ship(Texture texture, int x, int y) {
		sprite = new Sprite(texture);
		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setPosition(x, y);
		direction = targetDirection = new Vector2(0,-1);
		velocity = new Vector2(0,MIN_VELOCITY);
		setIsDrawable(true);
	}
	//assignment 1
	public void face(Vector2 targetPos)	{
		targetDirection = targetPos;
	}
	
	@Override
	public void update(float deltaTime) {
		double cosTheta = direction.dot(targetDirection) / targetDirection.len();
		if (cosTheta > 1.0) {
		  cosTheta = 1.0;
		}
		double deg = Math.acos(cosTheta);

		deg = Math.toDegrees(deg) * deltaTime;
		if (direction.crs(targetDirection) > 0) {
			deg = -deg;
		}
		sprite.rotate((float)deg);//rotate doesnt accept double args
		direction.rotate(-(float)deg);
		
		sprite.translate(velocity.x * deltaTime, velocity.y * deltaTime);
		if (velocity.len() > MIN_VELOCITY)
			velocity = velocity.scl(1 - deltaTime);
	}
	public Vector2 getDirection() {
		return direction;
	}
	public Vector2 getPosition() {
		return new Vector2(sprite.getX(),sprite.getY());
	}
	public void moveForward(float deltaTime) {
		velocity.x += direction.x * velocity.len() * deltaTime * 2;
		velocity.y -= direction.y * velocity.len() * deltaTime * 2;
		velocity.clamp(10, 80);
	}

}
