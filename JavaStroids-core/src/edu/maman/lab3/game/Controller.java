package edu.maman.lab3.game;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.math.Vector2;

import edu.maman.lab3.gameobjects.Asteroid;
import edu.maman.lab3.gameobjects.GameObject;
import edu.maman.lab3.gameobjects.Missile;
import edu.maman.lab3.gameobjects.Ship;

public class Controller {
	private float screenHeight, missileSecondTimer;
	ArrayList<GameObject> drawableObjects; 
	Ship ship;
	private Sound thrustersSound, missileSound;
	private Music backgroundNoise;
	
	public Controller(){
		drawableObjects = new ArrayList<GameObject>(); 
		initShip();
		initSound();
		initAsteroids(10);
		screenHeight = Gdx.graphics.getHeight();
	}
	private void initSound(){
		thrustersSound = Gdx.audio.newSound(Gdx.files.internal("125810__robinhood76__02578-rocket-start.wav"));
		backgroundNoise = Gdx.audio.newMusic(Gdx.files.internal("132150__soundsodd__interior-spaceship.mp3"));
		backgroundNoise.setLooping(true);
		backgroundNoise.play();
		backgroundNoise.setVolume(0.5F);
		missileSound = Gdx.audio.newSound(Gdx.files.internal("175261__jonccox__gun-laser.wav"));
	}
	
	private void initShip(){
		int w = Constants.SHIP_WIDTH; 
		int h = Constants.SHIP_HEIGHT; 
		Pixmap pmap = new Pixmap(w, h, Format.RGBA8888); // TODO: Check Image Format
		pmap.setColor(1, 1, 1, 1);
		pmap.drawLine(0, h, w / 2, 0);
		pmap.drawLine(w, h, w/2, 0);
		pmap.drawLine(1, h-1, w, h-1);
		ship = new Ship(new Texture(pmap), 100, 100);
		drawableObjects.add(ship);
	}
	private void initMissile()	{
		int w = Constants.SHIP_WIDTH;
		int h = Constants.SHIP_HEIGHT;
		Pixmap pmap = new Pixmap(w, h, Pixmap.Format.RGB565);
		pmap.setColor(1, 1, 1, 1);
		pmap.drawLine(w/2, 0, w/2, h);
		drawableObjects.add(new Missile(new Texture(pmap), ship.getDirection(),
				ship.getPosition()));
	}
	
	private void initAsteroids(int num){
		Random rand = new Random();
		for(int i = 0; i<num; i++){
			Asteroid asteroid = new Asteroid(new Texture("Asteroid_tex.png"));
			asteroid.sprite.setPosition(rand.nextInt(Gdx.graphics.getWidth()), rand.nextInt(Gdx.graphics.getHeight()));
			asteroid.sprite.setOrigin(asteroid.sprite.getWidth() / 2, asteroid.sprite.getHeight() / 2);
			asteroid.setRotVel(rand.nextFloat()*8-4);
			drawableObjects.add(asteroid);
		}
	}
	
	public void update() {
		processKeyboardInput();
		processMouseInput();
		float deltaT = Gdx.graphics.getDeltaTime();
		
		// Update Asteroids
		for(GameObject gObg : drawableObjects){
			if(gObg instanceof Asteroid){
				((Asteroid) gObg).update(deltaT); 
			}
			if(gObg instanceof Missile){
				((Missile) gObg).update(deltaT); //use appropriate updates
			}
		}
		ship.update(Gdx.graphics.getDeltaTime()); // Update ship
		missileSecondTimer += Gdx.graphics.getDeltaTime(); //update missile timer
	}
	private void processMouseInput() {
		if (Gdx.input.isButtonPressed(0)) {
			ship.face(new Vector2(Gdx.input.getX() - ship.sprite.getX(), 
					-(screenHeight - Gdx.input.getY() - ship.sprite.getY())));
		}
	}

	private void processKeyboardInput(){
		if (Gdx.app.getType() != ApplicationType.Desktop) return; // Just in case :)
		if (Gdx.input.isKeyPressed(Keys.UP)) 
			ship.moveForward(Gdx.graphics.getDeltaTime());
		if (Gdx.input.isKeyJustPressed(Keys.UP)) 
			thrustersSound.play(0.5f);
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)){
			if(missileSecondTimer>1.0) {
				initMissile();
				missileSound.play();
				missileSecondTimer=0;//reset to 0 upon fire (one missile/second max)
			}
		}
	}
	public void dispose() {
		if(thrustersSound != null) {
			thrustersSound.dispose();
		}
		if(backgroundNoise != null) {
			backgroundNoise.dispose();
		}
		if(missileSound != null) {
			missileSound.dispose();
		}
	}
	public ArrayList<GameObject> getDrawableObjects(){
		return drawableObjects;
	}
}
