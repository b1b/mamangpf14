package edu.maman.lab5.intro;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class IntroToLibGDX extends ApplicationAdapter{

	private SpriteBatch spriteBatch;
	private Sprite bug;
	private Sprite chest;
	private float rotDeg;
	private static float bugSpeed = 10.0f; // 10 pixels per second.
	private float bugX=0;
	private float bugY=0;
	private int state=0;
	private Vector2 dirAndVel;
	/*
	 * (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationAdapter#create()
	 */
	@Override
	public void create() {
		// Game Initialization  
		spriteBatch = new SpriteBatch(); 
		bug = new Sprite(new Texture("EnemyBug.png"));
		bug.setSize(50, 85);
		bug.setOrigin(bug.getWidth() / 2, bug.getHeight() / 2);
		bug.setPosition(0, 0);
		chest = new Sprite(new Texture("ChestClosed.png"));
		chest.setSize(50, 85);
		chest.setOrigin(chest.getWidth()/2, chest.getHeight()/2);
		chest.setPosition(270, 240);
		rotDeg = 3;
		dirAndVel = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        dirAndVel.nor();
        dirAndVel.scl(100);
        bug.rotate(dirAndVel.angle());
	}
	private static void go(Sprite s, float dT, Vector2 dirAndVel) {
    	s.translate(dirAndVel.x * dT, dirAndVel.y * dT);
    }
	@Override
	public void render() {
		// Game Loop
		/*bugX += bugSpeed;//Gdx.graphics.getDeltaTime() * bugSpeed;
		bugY += bugSpeed;//Gdx.graphics.getDeltaTime() * bugSpeed;*/
		float deltaT = Gdx.graphics.getDeltaTime();
		Gdx.gl.glClearColor(0.7f, 0.7f, 0.2f, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();
		//System.out.println(dirAndVel.angle());
		switch(state){
			case 0:
				go(bug,deltaT,dirAndVel);
				if(bug.getX() > (Gdx.graphics.getWidth() - 100)) {
					state = 1;
					dirAndVel.scl(-1);
				}
				break;
			case 1:
				bug.rotate(rotDeg);
				if(Math.abs(bug.getRotation() % Math.toDegrees(12) - (double)dirAndVel.angle()) < 12) {
	                state = 2;
	                bug.setRotation(dirAndVel.angle());
	            }
				break;
			case 2:
				go(bug,deltaT,dirAndVel);
				if(bug.getX() < 10.0) {
					state = 3;
					dirAndVel.scl((float)-1.0);
				}
				break;
			default://case 3
				bug.rotate(rotDeg);
	            if(Math.abs(bug.getRotation() % Math.toDegrees(12) - (double)dirAndVel.angle()) < 12) {
	                state = 0;
	                bug.setRotation(dirAndVel.angle());
	            }
				break;
		}
		
		bug.draw(spriteBatch);
		chest.draw(spriteBatch);
		spriteBatch.end();
	} 
	

	@Override
	public void dispose() {
		spriteBatch.dispose();
		super.dispose();
	}
}
