package edu.maman.lab5.gameobjects;

public interface Updatable {
	void update(float deltaTime);
}
