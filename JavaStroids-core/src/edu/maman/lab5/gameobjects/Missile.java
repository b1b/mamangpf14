package edu.maman.lab5.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Missile extends GameObject implements Updatable {
	private Vector2 dirAndVel;
	private final float VELOCITY = 100;
	int screenHeight, screenWidth;
	public boolean deleteMe;
	
	public Missile(Texture texture, Vector2 direction, Vector2 position) {
		sprite = new Sprite(texture);
		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setPosition(position.x, position.y);
	    dirAndVel = new Vector2(direction.x, -direction.y);
	    dirAndVel.scl(VELOCITY);
	    sprite.rotate(dirAndVel.angle() - 90);
		screenHeight = Gdx.graphics.getHeight();
		screenWidth = Gdx.graphics.getWidth();
		deleteMe=false;
		setIsDrawable(true);
	}
	public void update(float deltaT) {
		sprite.translate(dirAndVel.x * deltaT, dirAndVel.y * deltaT);//update the position of the missile 
		if (((sprite.getX() < 0) || (sprite.getY() < 0) || //the 4 different off screen cases
				sprite.getX() > screenWidth || 	sprite.getY() > screenHeight))
			deleteMe = true;
	}
}
