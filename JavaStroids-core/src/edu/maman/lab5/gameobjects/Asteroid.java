package edu.maman.lab5.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import edu.maman.lab5.game.Constants;

public class Asteroid extends GameObject implements Updatable{

	private float rotationalVel;
	private Vector2 dirAndVel;
	private int screenW, screenH;
	
	public Asteroid(Texture tex){
		sprite = new Sprite(tex);
		sprite.setSize(Constants.ASTEROIDS_SIZE, Constants.ASTEROIDS_SIZE); 
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		dirAndVel = new Vector2(0,(int)Math.random()%20);
		setIsDrawable(true);
		screenW=Gdx.graphics.getWidth();
		screenH=Gdx.graphics.getHeight();
	}
	
	@Override
	public void update(float deltaTime) {
		sprite.rotate(getRotVel()*deltaTime); // TODO: Student, use delta time here
		sprite.translate(dirAndVel.x*deltaTime, dirAndVel.y*deltaTime);
		
		if ((sprite.getX() < 0) || (sprite.getX() > screenW)) 
			dirAndVel.x = (-dirAndVel.x);
		if ((sprite.getY() < 0) || (sprite.getY() > screenH))
			dirAndVel.y = (-dirAndVel.y);//bounce!
	}
		
	public void setRotVel(float vel){
		rotationalVel = vel;
	}
	public float getRotVel(){
		return rotationalVel;
	}

}
