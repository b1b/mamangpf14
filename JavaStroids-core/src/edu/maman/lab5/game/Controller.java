package edu.maman.lab5.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import edu.maman.lab5.gameobjects.Asteroid;
import edu.maman.lab5.gameobjects.GameObject;
import edu.maman.lab5.gameobjects.Missile;
import edu.maman.lab5.gameobjects.Ship;

public class Controller {
	private float screenHeight, missileSecondTimer;
	ArrayList<GameObject> drawableObjects; 
	Ship ship;
	private Sound thrustersSound, missileSound;
	private Music backgroundNoise;
	private boolean shipCrashed, missileCrashed;
	private Sound explosionSound;
	private float explosionX, explosionY;
	
	public Controller(){
		drawableObjects = new ArrayList<GameObject>(); 
		initShip();
		initSound();
		initAsteroids(10);
		screenHeight = Gdx.graphics.getHeight();
		missileSecondTimer=1;
	}
	private void initSound(){
		thrustersSound = Gdx.audio.newSound(Gdx.files.internal("125810__robinhood76__02578-rocket-start.wav"));
		backgroundNoise = Gdx.audio.newMusic(Gdx.files.internal("132150__soundsodd__interior-spaceship.mp3"));
		backgroundNoise.setLooping(true);
		backgroundNoise.play();
		backgroundNoise.setVolume(0.5F);
		missileSound = Gdx.audio.newSound(Gdx.files.internal("175261__jonccox__gun-laser.wav"));
		explosionSound = Gdx.audio.newSound(Gdx.files.internal("199725__omar-alvarado__expl1.mp3"));
	}
	
	private void initShip(){
		int w = Constants.SHIP_WIDTH; 
		int h = Constants.SHIP_HEIGHT; 
		Pixmap pmap = new Pixmap(w, h, Format.RGBA8888); // TODO: Check Image Format
		pmap.setColor(1, 1, 1, 1);
		pmap.drawLine(0, h, w / 2, 0);
		pmap.drawLine(w, h, w/2, 0);
		pmap.drawLine(1, h-1, w, h-1);
		ship = new Ship(new Texture(pmap), 100, 100);
		drawableObjects.add(ship);
	}
	private void initMissile()	{
		int w = Constants.SHIP_WIDTH;
		int h = Constants.SHIP_HEIGHT;
		Pixmap pmap = new Pixmap(w, h, Pixmap.Format.RGBA8888);
		pmap.setColor(1, 1, 1, 1);
		pmap.drawLine(w/2, 0, w/2, h);
		drawableObjects.add(new Missile(new Texture(pmap), ship.getDirection(),
				ship.getPosition()));
	}
	
	private void initAsteroids(int num){
		Random rand = new Random();
		for(int i = 0; i<num; i++){
			Asteroid asteroid = new Asteroid(new Texture("Asteroid_tex.png"));
			asteroid.sprite.setPosition(rand.nextInt(Gdx.graphics.getWidth()), rand.nextInt(Gdx.graphics.getHeight()));
			asteroid.sprite.setOrigin(asteroid.sprite.getWidth() / 2, asteroid.sprite.getHeight() / 2);
			asteroid.setRotVel(rand.nextFloat()*8-4);
			drawableObjects.add(asteroid);
		}
	}
	
	public void update() {
		processKeyboardInput();
		processMouseInput();
		float deltaT = Gdx.graphics.getDeltaTime();
		Asteroid foo = new Asteroid(new Texture("Asteroid_tex.png"));
		Object bar = new Object();
		
		// Update Asteroids
		for(GameObject gObg : drawableObjects){
			if(gObg instanceof Asteroid){
				((Asteroid) gObg).update(deltaT); 
				if(ship.sprite.getBoundingRectangle().overlaps(((Asteroid) gObg).sprite.getBoundingRectangle()) && !shipCrashed){
					shipCrashed=true;
					explosionSound.play();
					thrustersSound.stop();
					explosionX=ship.sprite.getX();
					explosionY=ship.sprite.getY();
				}
			}
			if(gObg instanceof Missile){
				((Missile) gObg).update(deltaT); //use appropriate updates
				for(GameObject g2 : drawableObjects){
					if(g2 instanceof Asteroid){
						if(gObg.sprite.getBoundingRectangle().overlaps(((Asteroid) g2).sprite.getBoundingRectangle()) && !shipCrashed){
							missileCrashed=true;
							explosionSound.play();
							explosionX=g2.sprite.getX();
							explosionY=g2.sprite.getY();
							foo=(Asteroid)g2;
							bar = (Missile)gObg;
							break;
						}						
					}
				}
			}
			/*if(gObg instanceof Missile){
				((Missile) gObg).update(deltaT); //use appropriate updates
				
				for(Iterator<GameObject> iterator = drawableObjects.iterator(); iterator.hasNext();) {
					GameObject c = iterator.next();
					if(c instanceof Asteroid) {
						if((gObg.sprite.getBoundingRectangle().overlaps(c.sprite.getBoundingRectangle()))){
							explosionSound.play();
							explosionX=ship.sprite.getX();
							explosionY=ship.sprite.getY();
							removeUsLater.add(gObg);
							removeUsLater.add(c);
						}
					}
				}
				
			}//I give up*/
		}
		if(!shipCrashed) {
			ship.update(Gdx.graphics.getDeltaTime()); // Update ship
		}
		else {
			drawableObjects.remove(ship);
		}
		if(isMissileCrashed()) {
			drawableObjects.remove(foo);
			drawableObjects.remove(bar);
		}
		missileSecondTimer += Gdx.graphics.getDeltaTime(); //update missile timer
	}
	private void processMouseInput() {
		if (Gdx.input.isButtonPressed(0)) {
			ship.face(new Vector2(Gdx.input.getX() - ship.sprite.getX(), 
					-(screenHeight - Gdx.input.getY() - ship.sprite.getY())));
		}
	}

	private void processKeyboardInput(){
		if (Gdx.app.getType() != ApplicationType.Desktop) return; // Just in case :)
		if (Gdx.input.isKeyPressed(Keys.UP)) 
			ship.moveForward(Gdx.graphics.getDeltaTime());
		if (Gdx.input.isKeyJustPressed(Keys.UP)) 
			thrustersSound.play(0.5f);
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)){
			if(missileSecondTimer>1.0) {
				initMissile();
				missileSound.play();
				missileSecondTimer=0;//reset to 0 upon fire (one missile/second max)
			}
		}
	}
	public void dispose() {
		if(thrustersSound != null) {
			thrustersSound.dispose();
		}
		if(backgroundNoise != null) {
			backgroundNoise.dispose();
		}
		if(missileSound != null) {
			missileSound.dispose();
		}
	}
	public ArrayList<GameObject> getDrawableObjects(){
		return drawableObjects;
	}
	public Sound getExplosionSound() {
		return explosionSound;
	}
	public boolean isShipCrashed() {
		return shipCrashed;
	}
	public float getExplosionX(){
		return explosionX;
	}
	public float getExplosionY(){
		return explosionY;
	}
	public boolean isMissileCrashed() {
		return missileCrashed;
	}
}
