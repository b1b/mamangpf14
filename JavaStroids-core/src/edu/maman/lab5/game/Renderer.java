package edu.maman.lab5.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import edu.maman.lab5.gameobjects.GameObject;

public class Renderer {
	
	private SpriteBatch spriteBatch;
	private Controller control;
	BitmapFont font;
	Texture bg1, bg2, explosionSheet;
	float bg1XPos, bg2XPos, shipExplosionStateTime;
	Animation explosionAnim;
	TextureRegion [] explosionFrames;
	TextureRegion currentFrameExplosion;
	
	public Renderer(Controller c){
		control = c;
		spriteBatch = new SpriteBatch(); 
		font = new BitmapFont();
		bg1=new Texture("sound_of_space.jpg");
		bg2=new Texture("sound_of_space.jpg");
		bg1XPos=0;
		bg2XPos=bg1.getWidth();
		explosionSheet= new Texture (Gdx.files.internal("explosion17.png"));
		TextureRegion[][] tmp = TextureRegion.split(explosionSheet, explosionSheet.getWidth()/5, explosionSheet.getHeight()/5);
		explosionFrames = new TextureRegion[25];
		int index=0;
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				explosionFrames[index++] = tmp[i][j];
			}
		}
		explosionAnim=new Animation(0.040f, explosionFrames);
		shipExplosionStateTime = 0f;
	}
	
	public void render(){
		spriteBatch.begin();
		renderBackground();
		for(GameObject gObj : control.getDrawableObjects()){
			gObj.sprite.draw(spriteBatch);
		}
		if(control.isShipCrashed()&&!explosionAnim.isAnimationFinished(shipExplosionStateTime)){
			shipExplosionStateTime=Gdx.graphics.getDeltaTime();
			currentFrameExplosion = explosionAnim.getKeyFrame(shipExplosionStateTime, false);
			spriteBatch.draw(currentFrameExplosion, control.getExplosionX()-Constants.SHIP_WIDTH,
					control.getExplosionY()-Constants.SHIP_HEIGHT);
			
		}
		spriteBatch.end();
	}

	private void renderBackground() {
		// TODO Auto-generated method stub
		spriteBatch.draw(bg1, bg1XPos, 0);
		spriteBatch.draw(bg2, bg2XPos, 0);
		if (bg1XPos + bg1.getWidth() < Gdx.graphics.getWidth()) {
			bg2XPos = (bg1XPos + bg1.getWidth());
		}
		if (bg2XPos + bg2.getWidth() < Gdx.graphics.getWidth()) {
			bg1XPos = (bg2XPos + bg2.getWidth());
		}
		bg1XPos-=0.3;
		bg2XPos-=0.3;
		
	}

}
